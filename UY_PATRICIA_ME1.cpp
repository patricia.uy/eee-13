#include <iostream>
#include <cmath>
using namespace std;

void graycode(int n){
	int i = 0;
	int count = pow(2,n);

	for(; i < count; i++){
		int output = i ^ (i >> 1);
		cout << output;
		cout << endl; 
	}
}

int main(){
	int n; 
	cin >> n; 
	cout << endl;
	graycode(n);

	return 0; 
}