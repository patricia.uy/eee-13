#include <iostream>
using namespace std;

class Matrix{
  double I[4][4];
  public:
    // default constructor 
    Matrix(){
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          I[i][j] = 0;
        }
      }
    }

    // initialization
    Matrix(double x){
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          if(i == j){
            I[i][j] = x;
          }
          else{
            I[i][j] = 0; 
          }
        }
      }
    } 

    // copy constructor
    Matrix(const Matrix &I2){
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          I[i][j] = I2.I[i][j];
        }
      }
    }

    // overloading assignment operator
    const Matrix operator=(const Matrix &I2){
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          I[i][j] = I2.I[i][j];
        }
      }
      return I2;     
    }

    // overloading + operator
    const Matrix operator+(const Matrix& I2){
      Matrix sum; 
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          sum.I[i][j] = I[i][j] + I2.I[i][j];
        }
      }
      return sum; 
    }

    // overloading - operator
    const Matrix operator-(const Matrix &I2){
      Matrix diff;
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          diff.I[i][j] = I[i][j] - I2.I[i][j];
        }
      }
      return diff;
    }

    // overloading * operator for scalar multiplication
    const Matrix operator*(double x){
      Matrix scalar; 
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          scalar.I[i][j] = x * I[i][j];
        }
      }
      return scalar;
    }

    // overloading * operator for matrix multiplication
    const Matrix operator*(const Matrix &I2){
      Matrix product; 
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          for(int k = 0; k < 4; k++){
            product.I[i][j] += I[i][k] * I2.I[k][j];
          }
        }
      }
      return product; 
    }

    // get, set, print functions
    double get(int i, int j){
      return I[i][j]; 
    }

    void set(double input, int i, int j){
      I[i][j] = input;
    }

    void print(){
      for(int i = 0; i < 4; i++){
        for(int j = 0; j < 4; j++){
          cout << I[i][j];
          cout << " ";
        }
        cout << endl;
      }
    }
};

int main(){
}