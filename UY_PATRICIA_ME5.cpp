#include <iostream>
using namespace std;

int max(int a, int b){
  return (a > b)? a : b; 
}

int knapsack(int n, int val[], int weight[], int qty[], int cap){
  
}

int main(){
  int n; // number of items
  int val[100];
  int weight[100];
  int qty[100]; 
  int cap; // capacity of knapsack

  cin >> n; 

  for(int i = 0; i < n; i++){
    cin >> val[i];
    cin >> weight[i];
    cin >> qty[i];
  }

  cin >> cap; 

  cout << knapsack(n, val, weight, qty, cap);

  return 0; 
}