#include <iostream>

using namespace std;

void distribute(int weight[], int n, int m, int cap, int bin_sum[], int count_m){
  int solution[n];

for(int i = 0; i < n; i++){
    bin_sum[count_m] += weight[i];
    solution[i] = count_m;

    if(bin_sum[count_m] == cap && i != n-1){
      count_m += 1;
    }

    if(bin_sum[count_m] > cap){
      bin_sum[count_m] -= weight[i];
      bin_sum[count_m + 1] += weight[i];
      solution[i] = count_m + 1;

      if(bin_sum[count_m + 1] > cap){
        bin_sum[count_m + 1] -= weight[i];
        bin_sum[count_m + 2] += weight[i];
        solution[i] = count_m + 2;
      }
    }
  }

  for(int i = 0; i < m; i++){
    if(bin_sum[i] != cap){
      // remove the one on top, try to look for another one, repeat function?
    }
  }

  for(int i = 0; i < n; i++){
    cout << solution[i] << " ";
  }
  cout << endl;
}

int main(){
  int n; // number of objects
  int m; // number of bins

  cin >> n; 

  int weight[n]; // weights of objects
  int sum = 0; // sum of weights
  int cap; // bin capacity in terms of weight

  for(int i = 0; i < n; i++){
    cin >> weight[i];
    sum += weight[i];
  }

  cin >> m; 
  cap = sum / m;

  int bin_sum[m];
  int count_m = 1;

  for(int i = 0; i < m; i++){
    bin_sum[i] = 0;
  }

  distribute(weight, n, m, cap, bin_sum, count_m);
}