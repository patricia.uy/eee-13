#include <stdio.h>
#include <stdlib.h>

// get minimum element of array from index a to index b, inclusive
int get_min (int *array, int a, int b){
	int c = array[a]; 

	for(a; a < b; a++){
		if(array[a+1] < c){
			c = array[a+1];
		}
	}

	return c;
}

// get maximum element of array from index a to index b, inclusive
int get_max(int *array, int a, int b){
	int c = array[a]; 

	for(a; a < b; a++){
		if(c < array[a+1]){
			c = array[a+1];
		}
	}

	return c;
}

// get sum of odd digits of string number
// example sum_of_odd_digits("12351") == 10
int sum_of_odd_digits(const char *str){
	int sum = 0;
	int i = 0;
	int digit;

	while(str[i] != '\0'){
		digit = str[i] - '0';
		if(digit % 2 == 1){
			sum += digit; 
		}
		i++;
	}

	return sum;
}

int array[] = {1, 4, -1, 2, 5};
int main(){
	printf("%d %d %d\n",
		get_min(array,1,4),
		get_max(array,0,2),
		sum_of_odd_digits("12351")
	);
}