#include <iostream>
#include <stack>

using namespace std;

void print(int n, int arr[]){
  for(int i = 1; i < n-1; i++){
    cout << arr[i] << " ";
  }
  cout << endl;
}

void get_span(int n, int height[]){
  stack<int> st; 
  stack<int> st2;
  int spl[n];
  int spr[n];
  int output[n];

  st.push(0);
  st2.push(n-1);
  spl[0] = 1;
  spr[0] = 1;

  for(int i = 1; i < n; i++){
    // left 
    while(!st.empty() && height[st.top()] <= height[i]){
      st.pop();
    }

    if(st.empty()){
      spl[i] = i + 1; 
    }else{
      spl[i] = i - st.top();
    }

    st.push(i);

    // right
    while(!st2.empty() && height[st2.top()] <= height[(n-1)-i]){
      st2.pop();
    }

    if(st2.empty()){
      spr[i] = i + 1;
    }else{
      spr[i] = st2.top() - ((n-1)-i); 
    }
    
    st2.push((n-1)-i);
  }

  // adding both spans
  for(int i = 0; i < n; i++){
    output[i] = spl[i] + spr[(n-1)-i] - 1;
  }

  print(n, output);
}

int main(){
  int n; 
  cin >> n;

  int height[n];
  for(int i = 0; i < n; i++){
    cin >> height[i];
  } 

  get_span(n, height);
}